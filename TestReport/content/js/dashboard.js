/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8288043478260869, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.125, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.25, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.875, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.875, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.875, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.875, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-13"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-18"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-17"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-16"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/-15"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-5"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-7"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-10"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/-0"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-3"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/-2"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-19"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/-20"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [0.875, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 182, 0, 0.0, 430.0054945054948, 148, 4424, 180.5, 960.800000000002, 1386.0499999999997, 3906.079999999992, 3.2760327603276034, 85.55816026910269, 5.78649536495365], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/-9", 2, 0, 0.0, 416.0, 338, 494, 416.0, 494.0, 494.0, 494.0, 4.032258064516129, 899.5440083165323, 2.9730027721774195], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-8", 2, 0, 0.0, 236.0, 171, 301, 236.0, 301.0, 301.0, 301.0, 6.644518272425249, 38.893791528239205, 4.9249896179401995], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-23", 2, 0, 0.0, 174.5, 156, 193, 174.5, 193.0, 193.0, 193.0, 0.09926543577526305, 0.01202042386341076, 0.08898991215008933], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 2, 0, 0.0, 182.0, 180, 184, 182.0, 184.0, 184.0, 184.0, 0.09918175055789735, 0.012010290106620383, 0.08794631787751055], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 2, 0, 0.0, 192.5, 189, 196, 192.5, 196.0, 196.0, 196.0, 0.09909820632246556, 0.011903397829749283, 0.08835611559805769], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 2, 0, 0.0, 178.0, 149, 207, 178.0, 207.0, 207.0, 207.0, 0.09935419771485346, 0.012128197963238947, 0.08732302533532042], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 2, 0, 0.0, 169.0, 164, 174, 169.0, 174.0, 174.0, 174.0, 0.08056394763343404, 0.015577794561933535, 0.09881671701913394], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 2, 0, 0.0, 162.5, 153, 172, 162.5, 172.0, 172.0, 172.0, 0.08100445524503848, 0.009809133252328878, 0.10133467496962333], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 4, 0, 0.0, 1843.25, 1137, 2431, 1902.5, 2431.0, 2431.0, 2431.0, 0.14071129559925424, 4.357515522214796, 2.8793598515495833], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 2, 0, 0.0, 1655.5, 1391, 1920, 1655.5, 1920.0, 1920.0, 1920.0, 0.09263977025336977, 3.5190447102691187, 2.0225143591643895], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 2, 0, 0.0, 155.0, 150, 160, 155.0, 160.0, 160.0, 160.0, 0.0809814957282261, 0.00980635299833988, 0.10233403854719197], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 2, 0, 0.0, 160.5, 160, 161, 160.5, 161.0, 161.0, 161.0, 0.08099133392726979, 0.009886637442293674, 0.10100188811047218], "isController": false}, {"data": ["Test", 2, 0, 0.0, 9454.0, 8798, 10110, 9454.0, 10110.0, 10110.0, 10110.0, 0.19782393669634024, 235.07337258902078, 15.89855341246291], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 2, 0, 0.0, 155.0, 150, 160, 155.0, 160.0, 160.0, 160.0, 0.08101101749837979, 0.009730815578418666, 0.10197578276895658], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 2, 0, 0.0, 171.0, 163, 179, 171.0, 179.0, 179.0, 179.0, 0.08063215610385421, 0.009764050153201097, 0.10110516448959846], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 2, 0, 0.0, 172.5, 155, 190, 172.5, 190.0, 190.0, 190.0, 0.09929993545504195, 0.012121574152226802, 0.08737230648925079], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 2, 0, 0.0, 160.5, 150, 171, 160.5, 171.0, 171.0, 171.0, 0.0810142990237777, 0.009889440798800988, 0.10095141167416048], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 4, 0, 0.0, 281.0, 163, 622, 169.5, 622.0, 622.0, 622.0, 0.14614541468761416, 0.01769729630982828, 0.15014157837047862], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 4, 0, 0.0, 392.0, 157, 730, 340.5, 730.0, 730.0, 730.0, 0.14956625785222855, 2.587247957485791, 0.14438109949895303], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 4, 0, 0.0, 164.75, 152, 176, 165.5, 176.0, 176.0, 176.0, 0.1486381033778009, 0.017853990933075693, 0.15364593010293187], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 4, 0, 0.0, 395.0, 162, 631, 393.5, 631.0, 631.0, 631.0, 0.14618814414151013, 0.017631089649879396, 0.15218414224106425], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 4, 0, 0.0, 288.5, 156, 669, 164.5, 669.0, 669.0, 669.0, 0.1486712506968965, 0.017930565879947964, 0.1551320618844081], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 4, 0, 0.0, 283.75, 154, 645, 168.0, 645.0, 645.0, 645.0, 0.14864362690449648, 0.017999814195466368, 0.15350647993311037], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 4, 0, 0.0, 395.75, 151, 641, 395.5, 641.0, 641.0, 641.0, 0.1461240593263681, 0.017694710309052387, 0.14904939449842916], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 4, 0, 0.0, 282.75, 154, 657, 160.0, 657.0, 657.0, 657.0, 0.1496054157160489, 0.0180432312899727, 0.1526004460111456], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 4, 0, 0.0, 404.25, 233, 579, 402.5, 579.0, 579.0, 579.0, 0.15080681646810434, 1.746354716483185, 0.1163451025486352], "isController": false}, {"data": ["https://demowebshop.tricentis.com/", 2, 0, 0.0, 4112.0, 3800, 4424, 4112.0, 4424.0, 4424.0, 4424.0, 0.4520795660036166, 492.03165616523506, 7.960838607594936], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-14", 2, 0, 0.0, 282.5, 248, 317, 282.5, 317.0, 317.0, 317.0, 6.309148264984227, 8.822949526813881, 4.485410094637224], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-13", 2, 0, 0.0, 360.5, 343, 378, 360.5, 378.0, 378.0, 378.0, 4.975124378109452, 144.9053560323383, 3.755635883084577], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-12", 2, 0, 0.0, 649.5, 216, 1083, 649.5, 1083.0, 1083.0, 1083.0, 1.6891891891891893, 6.383947423986487, 1.2256519214527029], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-11", 2, 0, 0.0, 164.0, 158, 170, 164.0, 170.0, 170.0, 170.0, 11.76470588235294, 39.23483455882353, 8.513327205882351], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-18", 2, 0, 0.0, 181.0, 164, 198, 181.0, 198.0, 198.0, 198.0, 9.174311926605505, 27.46918004587156, 6.952408256880734], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-17", 2, 0, 0.0, 1099.5, 1024, 1175, 1099.5, 1175.0, 1175.0, 1175.0, 1.702127659574468, 732.6246675531914, 1.2466755319148937], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-16", 2, 0, 0.0, 1239.5, 1121, 1358, 1239.5, 1358.0, 1358.0, 1358.0, 1.4285714285714286, 77.77762276785715, 1.0463169642857144], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-15", 2, 0, 0.0, 716.5, 300, 1133, 716.5, 1133.0, 1133.0, 1133.0, 1.76522506619594, 12.870283539276258, 1.3101279788172993], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-5", 2, 0, 0.0, 682.5, 643, 722, 682.5, 722.0, 722.0, 722.0, 2.770083102493075, 6.465330678670361, 2.1343706717451525], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-4", 2, 0, 0.0, 713.0, 673, 753, 713.0, 753.0, 753.0, 753.0, 2.6560424966799467, 6.110972775564409, 2.0179697875166003], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-7", 2, 0, 0.0, 372.5, 355, 390, 372.5, 390.0, 390.0, 390.0, 5.050505050505051, 109.36020359848484, 3.684303977272727], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-6", 2, 0, 0.0, 1012.0, 995, 1029, 1012.0, 1029.0, 1029.0, 1029.0, 1.943634596695821, 179.28890306122452, 1.4140700923226435], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-1", 2, 0, 0.0, 338.0, 325, 351, 338.0, 351.0, 351.0, 351.0, 5.698005698005698, 521.8850160256411, 4.201166310541311], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-10", 2, 0, 0.0, 219.5, 190, 249, 219.5, 249.0, 249.0, 249.0, 8.032128514056224, 58.7898468875502, 5.89859437751004], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-0", 2, 0, 0.0, 1707.0, 1549, 1865, 1707.0, 1865.0, 1865.0, 1865.0, 1.0172939979654119, 35.366861648016275, 0.49076487792472023], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-3", 2, 0, 0.0, 802.5, 770, 835, 802.5, 835.0, 835.0, 835.0, 2.3228803716608595, 61.92381678281069, 1.7784552845528456], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-2", 2, 0, 0.0, 864.0, 847, 881, 864.0, 881.0, 881.0, 881.0, 2.2701475595913734, 56.61180476730988, 1.682658200908059], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 2, 0, 0.0, 399.5, 164, 635, 399.5, 635.0, 635.0, 635.0, 0.09776604585227551, 0.011838857114923986, 0.08659551131641981], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 2, 0, 0.0, 383.5, 156, 611, 383.5, 611.0, 611.0, 611.0, 0.09780907668231612, 0.011748551203051643, 0.0871112089201878], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 2, 0, 0.0, 154.5, 153, 156, 154.5, 156.0, 156.0, 156.0, 0.097799511002445, 0.011842909535452324, 0.08481051344743276], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 2, 0, 0.0, 162.0, 152, 172, 162.0, 172.0, 172.0, 172.0, 0.09772305286817161, 0.011833650933255155, 0.08436247923385126], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 2, 0, 0.0, 425.5, 344, 507, 425.5, 507.0, 507.0, 507.0, 0.09693209906460525, 3.3401816871031844, 0.07203645252750449], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 2, 0, 0.0, 231.0, 195, 267, 231.0, 267.0, 267.0, 267.0, 0.09807286814102878, 0.06618003113813563, 0.10477706811160692], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-19", 2, 0, 0.0, 257.5, 201, 314, 257.5, 314.0, 314.0, 314.0, 6.369426751592357, 47.37261146496815, 4.789510350318471], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 2, 0, 0.0, 176.5, 165, 188, 176.5, 188.0, 188.0, 188.0, 0.09774215619196559, 0.011740512901964617, 0.08466532474831395], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 2, 0, 0.0, 169.5, 150, 189, 169.5, 189.0, 189.0, 189.0, 0.09780907668231612, 0.011844067879499218, 0.08367260856807511], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 2, 0, 0.0, 158.0, 153, 163, 158.0, 163.0, 163.0, 163.0, 0.09777082518576456, 0.011743956540868206, 0.08335344764372311], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 2, 0, 0.0, 425.0, 166, 684, 425.0, 684.0, 684.0, 684.0, 0.09775648858692995, 0.01183769978982355, 0.08763716457304854], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-23", 2, 0, 0.0, 247.5, 240, 255, 247.5, 255.0, 255.0, 255.0, 4.040404040404041, 15.550031565656566, 3.0421401515151514], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-22", 2, 0, 0.0, 195.0, 194, 196, 195.0, 196.0, 196.0, 196.0, 4.597701149425287, 17.69037356321839, 3.5425646551724137], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 4, 0, 0.0, 161.25, 151, 173, 160.5, 173.0, 173.0, 173.0, 0.1486491508417258, 0.017927900516555798, 0.15162503716228773], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-21", 2, 0, 0.0, 280.5, 249, 312, 280.5, 312.0, 312.0, 312.0, 3.929273084479371, 15.176049852652259, 2.989163801571709], "isController": false}, {"data": ["https://demowebshop.tricentis.com/-20", 2, 0, 0.0, 264.5, 217, 312, 264.5, 312.0, 312.0, 312.0, 6.41025641025641, 24.67072315705128, 4.914112580128205], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 2, 0, 0.0, 155.0, 148, 162, 155.0, 162.0, 162.0, 162.0, 0.09861446674227109, 0.01194159558207189, 0.08378377545485924], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 2, 0, 0.0, 155.5, 154, 157, 155.5, 157.0, 157.0, 157.0, 0.09865338134464559, 0.011946307897203176, 0.08487659078577418], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 2, 0, 0.0, 390.5, 158, 623, 390.5, 623.0, 623.0, 623.0, 0.09774693318997116, 0.011836542690973072, 0.0843830946679048], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 2, 0, 0.0, 155.0, 151, 159, 155.0, 159.0, 159.0, 159.0, 0.08104056080068074, 0.009655223064143605, 0.09995530106568339], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 2, 0, 0.0, 397.0, 158, 636, 397.0, 636.0, 636.0, 636.0, 0.08068420203324189, 0.009770352589962885, 0.09888542339035017], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 2, 0, 0.0, 391.0, 163, 619, 391.0, 619.0, 619.0, 619.0, 0.09949753743594845, 0.012048529923884384, 0.08793483533157555], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 2, 0, 0.0, 465.5, 150, 781, 465.5, 781.0, 781.0, 781.0, 0.07914523149980213, 0.009583992876929166, 0.09877695884447962], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 2, 0, 0.0, 157.0, 156, 158, 157.0, 158.0, 158.0, 158.0, 0.09947279419078882, 0.019233997314234554, 0.08548443250770914], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 2, 0, 0.0, 152.5, 151, 154, 152.5, 154.0, 154.0, 154.0, 0.08114907084313884, 0.00974739815791609, 0.09763247585815142], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 2, 0, 0.0, 153.5, 152, 155, 153.5, 155.0, 155.0, 155.0, 0.09951734089665125, 0.01205092799920386, 0.08542552992984027], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 4, 0, 0.0, 402.5, 158, 649, 401.5, 649.0, 649.0, 649.0, 0.14781419755367503, 0.017755025682716825, 0.1507733685007945], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 2, 0, 0.0, 160.5, 157, 164, 160.5, 164.0, 164.0, 164.0, 0.0992950054612253, 0.011830069010028796, 0.0860104197696356], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 2, 0, 0.0, 157.5, 155, 160, 157.5, 160.0, 160.0, 160.0, 0.08107998540560263, 0.009818279482709693, 0.0988162322130782], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 2, 0, 0.0, 162.5, 158, 167, 162.5, 167.0, 167.0, 167.0, 0.09916208042044722, 0.011911070206752937, 0.0828933016014676], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 4, 0, 0.0, 405.5, 154, 674, 397.0, 674.0, 674.0, 674.0, 0.14617211766855473, 0.01770052987392655, 0.14902704184176868], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 2, 0, 0.0, 164.5, 152, 177, 164.5, 177.0, 177.0, 177.0, 0.09873617693522907, 0.011956333925750394, 0.08697268710505529], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 4, 0, 0.0, 278.75, 158, 632, 162.5, 632.0, 632.0, 632.0, 0.14617211766855473, 0.0176291567695962, 0.14767095285949206], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 2, 0, 0.0, 168.5, 156, 181, 168.5, 181.0, 181.0, 181.0, 0.09934432743890324, 0.012029977150804689, 0.08459790383469103], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 182, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
