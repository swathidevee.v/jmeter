/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 75.60975609756098, "KoPercent": 24.390243902439025};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.5515873015873016, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-5"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-4"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-1"], "isController": false}, {"data": [0.16666666666666666, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations"], "isController": false}, {"data": [0.5, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-23"], "isController": false}, {"data": [0.0, 500, 1500, "https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-23&currentTime=16:01"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 123, 30, 24.390243902439025, 761.1951219512193, 166, 5791, 318.0, 2293.8000000000015, 3670.3999999999996, 5699.800000000002, 2.647153771656085, 548.1650755272785, 2.4464608778112558], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login", 3, 0, 0.0, 5468.0, 5202, 5791, 5411.0, 5791.0, 5791.0, 5791.0, 0.5180452426178552, 2181.1011631734586, 1.7615561863236056], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-5", 3, 0, 0.0, 3495.0, 3191, 3686, 3608.0, 3686.0, 3686.0, 3686.0, 0.813890396093326, 1196.8703370862725, 0.4657614962018448], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-4", 3, 0, 0.0, 3655.0, 3360, 3806, 3799.0, 3806.0, 3806.0, 3806.0, 0.7836990595611285, 1201.5660205231193, 0.4561373432601881], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout", 3, 0, 0.0, 942.3333333333334, 847, 1051, 929.0, 1051.0, 1051.0, 1051.0, 0.10835801488116738, 1.1578293745033592, 0.48179105640034675], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-1", 3, 0, 0.0, 203.0, 196, 207, 206.0, 207.0, 207.0, 207.0, 14.084507042253522, 23.02486795774648, 8.087588028169014], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-0", 3, 0, 0.0, 1592.6666666666667, 1292, 1869, 1617.0, 1869.0, 1869.0, 1869.0, 1.6, 6.889583333333333, 0.821875], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-0", 3, 0, 0.0, 389.3333333333333, 315, 485, 368.0, 485.0, 485.0, 485.0, 0.2025384823116392, 0.3281360763232514, 0.16080447863219013], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-3", 3, 0, 0.0, 2317.0, 2167, 2427, 2357.0, 2427.0, 2427.0, 2427.0, 1.2360939431396785, 804.6066227595797, 0.7097883189122374], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-1", 3, 0, 0.0, 364.0, 329, 410, 353.0, 410.0, 410.0, 410.0, 0.2020338069903697, 0.8762032586369453, 0.12370624705367364], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/login-2", 3, 0, 0.0, 2192.6666666666665, 2014, 2365, 2199.0, 2365.0, 2365.0, 2365.0, 1.2647554806070826, 695.149876818086, 0.7385974388701518], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-2", 3, 0, 0.0, 244.33333333333334, 191, 305, 237.0, 305.0, 305.0, 305.0, 0.2039151712887439, 0.19734368627650897, 0.14397526254078305], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 3, 3, 100.0, 383.3333333333333, 310, 460, 380.0, 460.0, 460.0, 460.0, 0.20096463022508038, 0.06437148311897106, 0.10440740554662378], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate", 3, 0, 0.0, 1002.0, 886, 1087, 1033.0, 1087.0, 1087.0, 1087.0, 0.1953125, 2.1104812622070312, 0.9691238403320312], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-3", 3, 0, 0.0, 209.33333333333334, 191, 237, 200.0, 237.0, 237.0, 237.0, 0.2039151712887439, 0.1977419580954323, 0.146364893454323], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-4", 3, 0, 0.0, 242.33333333333334, 184, 305, 238.0, 305.0, 305.0, 305.0, 0.20401224073444407, 0.1978360889153349, 0.14444226028561713], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-5", 3, 0, 0.0, 205.0, 177, 238, 200.0, 238.0, 238.0, 238.0, 0.20410940263981495, 0.1981296349843516, 0.1463049819703361], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/validate-6", 3, 0, 0.0, 200.66666666666666, 166, 237, 199.0, 237.0, 237.0, 237.0, 0.20426227275822156, 0.19827802648600804, 0.14441981003608634], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 3, 3, 100.0, 440.3333333333333, 395, 517, 409.0, 517.0, 517.0, 517.0, 0.19964064683569574, 0.0639473946895588, 0.10001528498702336], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages", 9, 9, 100.0, 329.6666666666667, 307, 398, 325.0, 398.0, 398.0, 398.0, 0.22061527147934798, 0.22664772030886138, 0.11440108315970093], "isController": false}, {"data": ["Test", 3, 3, 100.0, 12201.666666666666, 11640, 13267, 11698.0, 13267.0, 13267.0, 13267.0, 0.22612497173437854, 960.520950125311, 4.878248779867341], "isController": true}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 3, 3, 100.0, 453.6666666666667, 307, 734, 320.0, 734.0, 734.0, 734.0, 0.20178919755162442, 0.06463560234075469, 0.10522991356696038], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push", 3, 0, 0.0, 1133.3333333333333, 901, 1331, 1168.0, 1331.0, 1331.0, 1331.0, 0.19401151134967343, 2.0713507647287073, 0.6809349333893812], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-5", 3, 0, 0.0, 207.33333333333334, 183, 238, 201.0, 238.0, 238.0, 238.0, 0.20569077819677753, 0.19966468117929378, 0.10766626671237572], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-4", 3, 0, 0.0, 242.66666666666666, 201, 290, 237.0, 290.0, 290.0, 290.0, 0.2057048820625343, 0.19947748817196928, 0.10586569613960504], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 3, 3, 100.0, 362.0, 312, 443, 331.0, 443.0, 443.0, 443.0, 0.20221083850094365, 0.0647706592073335, 0.10643715034375842], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-6", 3, 0, 0.0, 208.66666666666666, 187, 238, 201.0, 238.0, 238.0, 238.0, 0.20569077819677753, 0.19966468117929378, 0.10565756770654781], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 3, 3, 100.0, 345.0, 305, 412, 318.0, 412.0, 412.0, 412.0, 0.2008704385671242, 0.06434131235353197, 0.10278916973552059], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-1", 3, 0, 0.0, 440.3333333333333, 344, 512, 465.0, 512.0, 512.0, 512.0, 0.2025384823116392, 0.8522175854037267, 0.08485254776532541], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-0", 3, 0, 0.0, 446.0, 315, 614, 409.0, 614.0, 614.0, 614.0, 0.20165355918531963, 0.3267023971566848, 0.10122063420044364], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-3", 3, 0, 0.0, 202.0, 166, 238, 202.0, 238.0, 238.0, 238.0, 0.20569077819677753, 0.199463811278711, 0.10786713661295852], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/events/push-2", 3, 0, 0.0, 208.66666666666666, 187, 238, 201.0, 238.0, 238.0, 238.0, 0.20569077819677753, 0.1990620714775454, 0.10545669780596503], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-1", 3, 0, 0.0, 395.6666666666667, 326, 511, 350.0, 511.0, 511.0, 511.0, 0.11072562190890972, 0.4668714129512069, 0.06249942330405255], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-2", 3, 0, 0.0, 201.33333333333334, 183, 220, 201.0, 220.0, 220.0, 220.0, 0.11133377866844801, 0.1077458736918281, 0.07328024103763081], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-0", 3, 0, 0.0, 339.6666666666667, 298, 387, 334.0, 387.0, 387.0, 387.0, 0.11123882976751084, 0.18021994002373093, 0.06289773675330936], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-5", 3, 0, 0.0, 202.66666666666666, 188, 219, 201.0, 219.0, 219.0, 219.0, 0.11133377866844801, 0.10807204687152082, 0.07447620936317079], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-6", 3, 0, 0.0, 196.0, 167, 220, 201.0, 220.0, 220.0, 220.0, 0.11133791055854518, 0.1080760577101503, 0.07339168908888477], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-3", 3, 0, 0.0, 202.66666666666666, 187, 220, 201.0, 220.0, 220.0, 220.0, 0.11133791055854518, 0.10796732928187047, 0.07458770179996288], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/auth/logout-4", 3, 0, 0.0, 203.33333333333334, 189, 220, 201.0, 220.0, 220.0, 220.0, 0.11133377866844801, 0.10796332247828991, 0.07349768982409263], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-23", 3, 3, 100.0, 356.3333333333333, 309, 437, 323.0, 437.0, 437.0, 437.0, 0.20078977310755639, 0.06431547419851415, 0.10725781825179037], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-23&currentTime=16:01", 3, 3, 100.0, 326.3333333333333, 304, 352, 323.0, 352.0, 352.0, 352.0, 0.20255215718047398, 0.06487998784687057, 0.11848509975693741], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: code expected to contain /200/", 9, 30.0, 7.317073170731708], "isController": false}, {"data": ["401/Unauthorized", 21, 70.0, 17.073170731707318], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 123, 30, "401/Unauthorized", 21, "Test failed: code expected to contain /200/", 9, "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/subunit", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/buzz/feed?limit=5&offset=0&sortOrder=DESC&sortField=share.createdAtUtc", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/core/i18n/messages", 9, 9, "Test failed: code expected to contain /200/", 9, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/locations", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/action-summary", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/shortcuts", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/leaves?date=2023-01-23", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["https://opensource-demo.orangehrmlive.com/web/index.php/api/v2/dashboard/employees/time-at-work?timezoneOffset=5.5&currentDate=2023-01-23&currentTime=16:01", 3, 3, "401/Unauthorized", 3, "", "", "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
